CREATE DATABASE myfilmsdb;
USE myfilmsdb;
CREATE TABLE films_lists (
	id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(30) NOT NULL,
    hash VARCHAR(50)
);

CREATE TABLE films(
	id INT NOT NULL,
    list_id INT,
    FOREIGN KEY (list_id) REFERENCES films_lists(id)
);