package models;

import com.avaje.ebean.Model;
import javax.persistence.Entity;

@Entity
public class Film extends Model {
    private int filmId;
    private int listId;

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public int getListId() {
        return listId;
    }

    public void setListId(int listId) {
        this.listId = listId;
    }
}
