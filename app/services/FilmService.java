package services;

import models.Film;

import java.util.List;

public interface FilmService {
    void addFilmToList(Film film);

    boolean isFavoriteFilm(int filmId);

    List<Film> getFavoriteFilms(int listId);
}
