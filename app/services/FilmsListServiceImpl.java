package services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import dao.FilmsListDao;
import models.FilmsList;

import java.util.List;

@Singleton
public class FilmsListServiceImpl implements FilmsListService {

    @Inject
    private FilmsListDao filmsListDao;

    @Override
    public int createFilmsList(FilmsList list) {
        return filmsListDao.createFilmsList(list);
    }

    @Override
    public List<FilmsList> getAllLists() {
        return filmsListDao.getAllLists();
    }

    @Override
    public FilmsList getFilmsList(int listId) {
        return filmsListDao.getFilmsList(listId);
    }
}
