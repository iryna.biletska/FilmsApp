package services;

import models.FilmsList;

import java.util.List;

public interface FilmsListService {

    int createFilmsList(FilmsList list);

    List<FilmsList> getAllLists();

    FilmsList getFilmsList(int listId);
}
