package services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import dao.FilmsDao;
import models.Film;

import java.util.List;

@Singleton
public class FilmServiceImpl implements FilmService {

    @Inject
    private FilmsDao filmsDao;

    @Override
    public void addFilmToList(Film film) {
        filmsDao.addFilmToList(film);
    }

    @Override
    public boolean isFavoriteFilm(int filmId) {
        return filmsDao.isFavoriteFilm(filmId);
    }

    @Override
    public List<Film> getFavoriteFilms(int listId) {
        return filmsDao.getFavoriteFilms(listId);
    }
}
