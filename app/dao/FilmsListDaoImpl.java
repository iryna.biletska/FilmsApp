package dao;

import com.avaje.ebean.Ebean;
import models.FilmsList;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FilmsListDaoImpl implements FilmsListDao {

    @Override
    public int createFilmsList(FilmsList list) {
        list.save();
        return list.getId();
    }

    @Override
    public List<FilmsList> getAllLists() {
        return Ebean.find(FilmsList.class).findList();
    }

    @Override
    public FilmsList getFilmsList(int listId) {
        return Ebean.find(FilmsList.class).where().eq("id", listId).findUnique();
    }
}
