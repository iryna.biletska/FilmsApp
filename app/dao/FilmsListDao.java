package dao;

import com.avaje.ebean.Ebean;
import models.FilmsList;

import java.util.List;

public interface FilmsListDao {

    int createFilmsList(FilmsList list);

    List<FilmsList> getAllLists();

    FilmsList getFilmsList(int listId);
}
