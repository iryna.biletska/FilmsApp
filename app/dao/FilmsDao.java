package dao;

import com.avaje.ebean.Ebean;
import models.Film;

import java.util.List;

public interface FilmsDao {
    List<Film> getFavoriteFilms(int listId);

    void addFilmToList(Film film);

    boolean isFavoriteFilm(int filmId);
}
