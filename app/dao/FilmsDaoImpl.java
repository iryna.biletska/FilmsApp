package dao;

import com.avaje.ebean.Ebean;
import models.Film;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FilmsDaoImpl implements FilmsDao {

    @Override
    public List<Film> getFavoriteFilms(int listId) {
        return Ebean.find(Film.class).where().eq("list_id", listId).findList();
    }

    @Override
    public void addFilmToList(Film film) {
        film.save();
    }

    @Override
    public boolean isFavoriteFilm(int filmId) {
        return Ebean.find(Film.class, filmId) != null;
    }
}
