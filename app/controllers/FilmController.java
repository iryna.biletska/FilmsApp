package controllers;

import com.google.inject.Inject;
import models.Film;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.FilmServiceImpl;

public class FilmController extends Controller {

    @Inject
    private FilmServiceImpl filmService;

    public Result addFilmToList() {
        DynamicForm form = Form.form().bindFromRequest();
        Film film = new Film();
        film.setFilmId(Integer.parseInt(form.get("filmId")));
        film.setListId(Integer.parseInt(form.get("filmsListId")));

        filmService.addFilmToList(film);
        return ok();
    }

    public Result isFavoriteFilm(int filmId) {
        boolean isFavorite = filmService.isFavoriteFilm(filmId);
        return ok(Json.toJson(isFavorite));
    }
}
