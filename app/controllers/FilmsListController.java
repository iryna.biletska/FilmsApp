package controllers;

import com.google.inject.Inject;
import models.Film;
import models.FilmsList;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import services.FilmService;
import services.FilmsListService;

import java.util.List;

public class FilmsListController extends Controller{

    @Inject
    private FilmsListService filmsListService;

    @Inject
    private FilmService filmService;

    public Result createFilmsList() {
        DynamicForm form = Form.form().bindFromRequest();
        FilmsList list = new FilmsList();
        list.setName(form.get("filmsListName"));

        int listId = filmsListService.createFilmsList(list);
        return ok(Json.toJson(listId));
    }

    public Result getFilmsList(int id) {
        List<Film> films = filmService.getFavoriteFilms(id);
        return ok(Json.toJson(films));
    }

    public Result getAllFilmsLists() {
        List<FilmsList> filmsLists = filmsListService.getAllLists();
        return ok(Json.toJson(filmsLists));
    }

    public Result getFilmsListData(int listId) {
        return ok(Json.toJson(filmsListService.getFilmsList(listId)));
    }
}
