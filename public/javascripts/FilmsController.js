var filmsApplication = angular.module('FilmsApp', ['ngRoute']);

filmsApplication.config(function($routeProvider)
{
    $routeProvider
        .when('/films', {templateUrl : 'assets/pages/main.html'})
        .when('/filmslist', {templateUrl : 'assets/pages/films-list.html'})
        .when('/newlist', {templateUrl : 'assets/pages/films-list-new.html'})
        .when('/favfilm/:id', {templateUrl : 'assets/pages/film-favorite.html'})
        .when('/filmslist/view/:id', {templateUrl : 'assets/pages/films-list-view.html'})
        .otherwise({redirectTo: '/films'});
});

filmsApplication.controller('FilmsController', function($scope, $location, $http, $routeParams) {
    $scope.searchFilms = function(film) {
        $http({
            method: 'GET',
            url: "https://api.themoviedb.org/3/search/movie?api_key=7a4de0fe5da237bdb52d1168dae8cd14&query=" + encodeURIComponent(film.title)
        }).then(function successCallback(response) {
            $scope.filmsArray = response.data.results;
        });
    };

    $scope.getAllFilmsLists = function() {
        $http({
            method: 'GET',
            url: "/filmslist/all"
        }).then(function successCallback(response) {
            $scope.allFilmsLists = response.data;
        });
    };

    $scope.createFilmsList = function() {
        $http({
            method: 'POST',
            url: "/filmslist/create",
            data: {filmsListName:this.filmsListName}
        }).then(function successCallback(response) {
            $location.path('/');
        });
    };

    $scope.isFavoriteFilm = function(filmId) {
        $http({
            method: 'GET',
            url: "/film/isFavorite/" + filmId
        }).then(function successCallback(response) {
            if (response.data) {
                $('#favorite-' + filmId).show();
                $('#not-favorite-' + filmId).hide();
            }
        });
    };

    $scope.addFilmToFavorites = function() {
        var listId = $scope.selectedFilmsList;
        if (listId) {
            var filmId = $routeParams.id;
            $http({
                method: 'POST',
                url: '/film/add/favorite',
                data: {filmsListId: listId, filmId: filmId}
            }).then(function successCallback(response) {
                $location.path('/');
            });
        }
    };

    $scope.getFavoriteFilms = function() {
        var listId = $routeParams.id;
        $http({
            method: 'GET',
            url: "/filmslist/get/films/" + listId
        }).then(function successCallback(response) {
            $scope.favoriteFilms = response.data;
        });
    };

    $scope.getFilmsListData = function() {
        var listId = $routeParams.id;
        $http({
            method: 'GET',
            url: "/filmslist/get/info/" + listId
        }).then(function successCallback(response) {
            $scope.listData = response.data;
        });
    };

    $scope.getFilmData = function(filmData) {
        $http({
            method: 'GET',
            url: 'http://api.themoviedb.org/3/movie/' + filmData.filmId + '?api_key=7a4de0fe5da237bdb52d1168dae8cd14'
        }).then(function successCallback(response) {
            filmData.title = response.data.title;
            filmData.poster_path = response.data.poster_path;
        });
    }

    $scope.go = function ( path ) {
        $location.path(path);
    };
});